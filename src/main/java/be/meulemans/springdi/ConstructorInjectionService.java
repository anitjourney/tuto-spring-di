package be.meulemans.springdi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConstructorInjectionService {

    private final MyDependency myDependency;

    @Autowired
    public ConstructorInjectionService(MyDependency myDependency) {
        this.myDependency = myDependency;
    }

    public String foo() {
        return myDependency.generate() + " thanks constructor!";
    }
}
