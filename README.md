# Dependency Injection (DI) with Spring

One of the functionalities provide by Spring is the easy [injection of dependencies](https://en.wikipedia.org/wiki/Dependency_injection).

## Injection methods

There are 3 ways to inject dependencies using Spring:
 - Constructor
 - Setter
 - Field

### Field

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FieldInjectionService {

    @Autowired
    private MyDependency myDependency;

    public String foo() {
        return myDependency.generate() + " thanks field!";
    }

}
```


### Setter

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SetterInjectionService {

    private MyDependency myDependency;

    @Autowired
    public void setMyDependency(MyDependency myDependency) {
        this.myDependency = myDependency;
    }

    public String foo() {
        return myDependency.generate() + " thanks setter!";
    }

}
```

### Constructor

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConstructorInjectionService {

    private final MyDependency myDependency;

    @Autowired
    public ConstructorInjectionService(MyDependency myDependency) {
        this.myDependency = myDependency;
    }

    public String foo() {
        return myDependency.generate() + " thanks constructor!";
    }
}
```

### Best practice

The best choice is to always inject your dependencies through the constructor. This is even the official recommendation
 made by the Spring itself since version 4.3.
Here are some reasons why.

#### Class invariants
The injection through the constructor allows you to declare you dependencies as immutable (using the final keyword).
It also allows you to ensure that your class invariant are checked upon construction and always respected.

Thanks to this, you can never have an incoherent instance of this class. This is possible with the field or the setter 
injection methods since the object is first created in a first step and then initialized in a second step.

#### Dependency Hiding
With the injection through the constructor, each and every dependency is explicitly requested. 
Therefore, there is not much room for hidden ones. Furthermore, the constructor serves as an 'alway up to date' documentation.

#### Single Responsibility Principle
 Since every dependency is provided as a parameter of the constructor, if you have too many of them, 
 it is a clear indicator that your class has grown more than a single responsibility. With a setter or a field injection, 
 it might be harder to spot such a bad smell.

#### DI Container Coupling
 The managed class should not depend on the DI framework. If field injection is used, you cannot create a valid
 instance of the class without either through the DI framework, or using reflection. Using constructor injection
 lets you instantiate the class independently as long as you provide the required dependencies.
